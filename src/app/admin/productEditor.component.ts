import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Product } from "../model/product.model";
import { ProductRepository } from "../model/product.repository";

@Component({
	templateUrl: "productEditor.component.html"
})

export class ProductEditorComponent {
	subcat: any = null;
	editing: boolean = false;
	product: Product = new Product();
	constructor(private repository: ProductRepository,
		private router: Router,
		activeRoute: ActivatedRoute) {
		this.editing = activeRoute.snapshot.params["mode"] == "edit";
		if (this.editing) {
			Object.assign(this.product,
				repository.getProduct(activeRoute.snapshot.params["id"]));
		}
	}
	save(form: NgForm) {
		this.repository.saveProduct(this.product);
		this.router.navigateByUrl("/admin/main/products");
	}
	
	categories: any = [

		{
			category: 'Овощи, фрукты, зелень',
			subcategories: [
				'Фрукты и ягоды',
				'Овощи',
				'Зелень и салаты',
				'Грибы и соленья'
			]
		},
		{
			category: 'Хлеб, кондитерские изделия',
			subcategories: [
				'Хлеб, лаваш, лепешки',
				'Булочки и сладкая выпечка',
				'Шоколад, конфеты, жевательная резинка',
				'Торты и пирожные',
				'Мармелад, зефир, пастила',
				'Баранки, сушки, сухари',
				'Халва, лукум, восточные сладости',
				'Пироги и пирожки',
				'Кексы и рулеты',
			]
		},
		{
			category: 'Молочные продукты, яйца',
			subcategories: [
				'Молоко, сливки',
				'Кисломолочные продукты',
				'Йогурт',
				'Сыры',
				'Коктейли, десерты, каши',
				'Майонез',
				'Сгущённое молоко и сгущёнка',
				'Соевые продукты',
				'Яйца',
			]
		},
		{
			category: 'Мясо, рыба, птица',
			subcategories: [
				'Мясо',
				'Рыба',
				'Икра',
				'Морепродукты',
				'Птица',
			]
		},
		{
			category: 'Бакалея',
			subcategories: [
				'Крупы и макаронные изделия',
				'Растительные масла, соусы',
				'Чай, кофе, какао',
				'Орехи, сухофрукты, семечки',
				'Мука и кулинарные ингридиенты',
				'Соль, сахар, специи',
				'Диетические продукты',
				'Чипсы и снеки',
				'Консервы и мёд',
				'Продукты и быстрого приготовления',
			]
		},
		{
			category: 'Полуфабрикаты, замороженные продукты',
			subcategories: [
				'Пельмени, вареники, хинкали',
				'Замороженная рыба и морепродукты',
				'Пицца',
				'Мороженое, десерты',
				'Замороженные фрукты, овощи, смеси',
				'Замороженные блинчики, тесто и выпечка',
				'Замороженные готовые блюда и полуфабрикаты',
			]
		},
		{
			category: 'Напитки, воды, соки',
			subcategories: [
				'Соки, нектары',
				'Вода',
				'Лимонады, тоники, газированные напитки',
				'Морсы и компоты',
				'Ореховые и злаковые напитки',
				'Замороженные блинчики, тесто и выпечка',
				'Квас',
				'Холодный чай, кофе',
				'Энергетики и обогащенные напитки',
				'Безалкогольные пиво, вино',
			]
		},
		{
			category: 'Детское питание',
			subcategories: [
			]
		},
		{
			category: 'Алкоголь',
			subcategories: [
				'Вино',
				'Пиво',
				'Крепкий алкоголь',
				'Слабоалкогольные напитки',
			]
		},
		{
			category: 'Товары для зверей',
			subcategories: [
				'Косметика и гигиена',
				'Уборка, бытовая химия',
				'Дом, хобби, техника',
				'Автомобиль и дача',
				'Канцтовары и печатная продукция',
				'Гардероб и текстиль',
				'Бестселлеры',
			]
		},
		{
			category: 'Другое',
			subcategories: [
				'Хлеб, лаваш, лепешки',
				'Булочки и сладкая выпечка',
				'Шоколад, конфеты, жевательная резинка',
				'Торты и пирожные'
			]
		}
	];
	
}