import { Component } from "@angular/core";
import { Product } from "../model/product.model";
import { ProductRepository } from "../model/product.repository";

@Component({
    templateUrl: "productTable.component.html"
})
export class ProductTableComponent {

    public productsPerPage = 10;
	public selectedPage = 1;

    constructor(private repository: ProductRepository) { }

    getProducts(): Product[] {
        let pageIndex = (this.selectedPage - 1) * this.productsPerPage
		return this.repository.getProducts()
			.slice(pageIndex, pageIndex + this.productsPerPage);
    }
    deleteProduct(id: number) {
        this.repository.deleteProduct(id);
    }
    changePage(newPage: number) {
		this.selectedPage = newPage;
	}
	changePageSize(newSize: number) {
		this.productsPerPage = Number(newSize);
		this.changePage(1);
	}
	get pageCount(): number {
		return Math.ceil(this.repository
			.getProducts().length / this.productsPerPage)
	}

}