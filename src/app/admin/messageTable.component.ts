import { Component } from "@angular/core";
import { Message } from "../model/message.model";
import { MessageRepository } from "../model/message.repository";

@Component({
    templateUrl: "messageTable.component.html"
})
export class MessageTableComponent {
    constructor(private repository: MessageRepository) { }
    getMessages(): Message[] {
        return this.repository.getMessages();
    }
    delete(id: number) {
        this.repository.deleteMessage(id);
    }
}