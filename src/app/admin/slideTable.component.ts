import { Component } from "@angular/core";
import { Slide } from "../model/slide.model";
import { SlideRepository } from "../model/slide.repository";
import { NgForm } from "@angular/forms";

@Component({
    templateUrl: "slideTable.component.html"
})
export class SlideTableComponent {
    private editedSlide: Slide = null;

    constructor(private repository: SlideRepository) { }

    getSlides(): Slide[] {
        return this.repository.getSlides();
    }
    save(form: NgForm) {
        this.repository.updateSlide(this.editedSlide);
    }
}