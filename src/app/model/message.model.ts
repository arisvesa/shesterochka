import { Injectable } from "@angular/core";

@Injectable()
export class Message {
    public id: number;
    public name: string;
    public mail: string;
    public phone: string;
    public content: string;
    public feedback: any;
	constructor() { }
	clear() {
        this.id = null;
        this.name = null;
        this.mail = null;
        this.phone = null;
		this.content = null;
        this.feedback = null;
	}
}