import { Injectable } from "@angular/core";
import { Product } from "./product.model";
import { Slide } from "./slide.model";
import { Observable, from } from "rxjs";
import { Order } from "./order.model";
import { Message } from "./message.model";

@Injectable()
export class StaticDataSource {
    private products: Product[] = [
        new Product(1, "Product 1", 100, "Category1", "Desc", "Comp", "En", "St"),
        new Product(2, "Product 2", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(3, "Product 3", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(4, "Product 4", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(5, "Product 5", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(6, "Product 6", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(7, "Product 7", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(8, "Product 8", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(9, "Product 9", 100, "Category2", "Desc", "Comp", "En", "St"),
        new Product(10, "Product 10", 100, "Category1", "Desc", "Comp", "En", "St"),
        new Product(11, "Product 11", 100, "Category3", "Desc", "Comp", "En", "St"),
        new Product(12, "Product 12", 100, "Category3", "Desc", "Comp", "En", "St"),
        new Product(13, "Product 13", 100, "Category3", "Desc", "Comp", "En", "St"),
        new Product(14, "Product 14", 100, "Category1", "Desc", "Comp", "En", "St"),
        new Product(15, "Product 15", 100, "Category2", "Desc", "Comp", "En", "St"),
    ];

    private slides: Slide[] = [
        new Slide(1, "assets/slides/1.jpg", ""),
        new Slide(2, "assets/slides/1.jpg", ""),
        new Slide(3, "assets/slides/1.jpg", ""),
    ];

    getProducts(): Observable<Product[]> {
        return from([this.products]);
    }

    getSlides(): Observable<Slide[]> {
        return from([this.slides]);
    }

    saveOrder(order: Order): Observable<Order> {
        console.log(JSON.stringify(order));
        return from([order]);
    }

    saveMessage(message: Message): Observable<Message> {
        console.log(JSON.stringify(message));
        return from([message]);
    }
}
