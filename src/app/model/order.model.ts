import { Injectable } from "@angular/core";
import { Cart } from "./cart.model";

@Injectable()
export class Order {
	public id: number;
	public name: string;
	public adress: string;
	public city: string;
	public state: string;
	public phone: string;
	public mail: string;
	public shipped: boolean = false;
	constructor(public cart: Cart) { }
	clear() {
		this.id = null;
		this.name = null;
		this.adress = null;
		this.city = null;
		this.state = null;
		this.shipped = false;
		this.cart.clear();
	}
}