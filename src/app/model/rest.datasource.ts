import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Product } from "./product.model";
import { Order } from "./order.model";
import { Message } from "./message.model";
import { Slide } from "./slide.model";
import { map } from "rxjs/operators";
import { HttpHeaders } from '@angular/common/http';

const PROTOCOL = "http";
const PORT = 3500;

@Injectable()
export class RestDataSource {
	baseUrl: string;
	auth_token: string;

	constructor(private http: HttpClient) {
		this.baseUrl = `${PROTOCOL}://${location.hostname}:${PORT}/`;
	}


	getProducts(): Observable<Product[]> {
		return this.http.get<Product[]>(this.baseUrl + "products");
	}
	saveProduct(product: Product): Observable<Product> {
		return this.http.post<Product>(this.baseUrl + "products",
			product, 
			this.getOptions());
	}
	updateProduct(product): Observable<Product> {
		return this.http.put<Product>(`${this.baseUrl}products/${product.id}`,
			product, 
			this.getOptions());
	}
	deleteProduct(id: number): Observable<Product> {
		return this.http.delete<Product>(`${this.baseUrl}products/${id}`,
			this.getOptions());
	}


	getSlides(): Observable<Slide[]> {
		return this.http.get<Slide[]>(this.baseUrl + "slides");
	}
	saveSlide(slide: Slide): Observable<Slide> {
		return this.http.post<Slide>(this.baseUrl + "slides", 
			slide,
			this.getOptions());
	}
	updateSlide(slide: Slide): Observable<Slide> {
		return this.http.put<Slide>(`${this.baseUrl}slides/${slide.id}`,
			slide,
			this.getOptions());
	}
	deleteSlide(id: number): Observable<Slide> {
		return this.http.delete<Slide>(`${this.baseUrl}slides/${id}`,
			this.getOptions());
	}


	getOrders(): Observable<Order[]> {
		return this.http.get<Order[]>(this.baseUrl + "orders", 
			this.getOptions());
	}
	saveOrder(order: Order): Observable<Order> {
		return this.http.post<Order>(this.baseUrl + "orders", 
			order);
	}
	updateOrder(order: Order): Observable<Order> {
		return this.http.put<Order>(`${this.baseUrl}orders/${order.id}`, 
			order,
			this.getOptions());
	}
	deleteOrder(id: number): Observable<Order> {
		return this.http.delete<Order>(`${this.baseUrl}orders/${id}`,
			this.getOptions());
	}


	getMessages(): Observable<Message[]> {
		return this.http.get<Message[]>(this.baseUrl + "messages", 
		this.getOptions());
	}
	saveMessage(message: Message): Observable<Message> {
		return this.http.post<Message>(this.baseUrl + "messages", 
		message);
	}
	updateMessage(message: Message): Observable<Message> {
		return this.http.put<Message>(`${this.baseUrl}messages/${message.id}`, 
			message,
			this.getOptions());
	}
	deleteMessage(id: number): Observable<Message> {
		return this.http.delete<Message>(`${this.baseUrl}messages/${id}`,
			this.getOptions());
	}


	authenticate(user: string, pass: string): Observable<boolean> {
		return this.http.post<any>(this.baseUrl + "login", {
			name: user, password: pass
		}).pipe(map(response => {
			this.auth_token = response.success ? response.token : null;
			return response.success;
		}));
	}
	private getOptions() {
		return {
			headers: new HttpHeaders({
				"Authorization": `Bearer<${this.auth_token}>`
			})
		}
	}
}