import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Message } from "./message.model";
//import { StaticDataSource } from "./static.datasource";
import { RestDataSource } from "./rest.datasource";

@Injectable()
	export class MessageRepository {
	private messages: Message[] = [];
	private loaded: boolean = false;
	constructor(private dataSource: RestDataSource) {}

	loadMessages() {
		this.loaded = true;
		this.dataSource.getMessages()
			.subscribe(messages => this.messages = messages);
	}

	getMessages(): Message[] {
		if (!this.loaded) {
			this.loadMessages();
		}
		return this.messages;
	}
	
	saveMessage(message: Message): Observable<Message> {
		return this.dataSource.saveMessage(message);
	}

	updateMessage(message: Message) {
		this.dataSource.updateMessage(message).subscribe(message => {
			this.messages.splice(this.messages.
				findIndex(m => m.id == message.id), 1, message);
		});
	}

	deleteMessage(id: number) {
		this.dataSource.deleteMessage(id).subscribe(message => {
			this.messages.splice(this.messages.findIndex(m => id == m.id), 1);
		});
	}
}