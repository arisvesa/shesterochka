export class Product {
 constructor(
	 public id?: number,
	 public title?: string,
	 public price?: number,
	 public src?: string,
	 public categories?: any,
	 public subCat?: any,
	 public desc?: string,
	 public comp?: string,
	 public energy?: string,
	 public storing?: string) { }
}