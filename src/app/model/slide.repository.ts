import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Slide } from "./slide.model";
//import { StaticDataSource } from "./static.datasource";
import { RestDataSource } from "./rest.datasource";

@Injectable()
export class SlideRepository {
	private slides: Slide[] = [];
	
	constructor(private dataSource: RestDataSource) {
		dataSource.getSlides().subscribe(data => {
             this.slides = data;
         });
	}

	getSlides(): Slide[] {
		return this.slides;
	}
	saveSlide(slide: Slide): Observable<Slide> {
		return this.dataSource.saveSlide(slide);
	}

	updateSlide(slide: Slide) {
		this.dataSource.updateSlide(slide).subscribe(slide => {
			this.slides.splice(this.slides.
				findIndex(o => o.id == slide.id), 1, slide);
		});
	}

	deleteSlide(id: number) {
		this.dataSource.deleteSlide(id).subscribe(slide => {
			this.slides.splice(this.slides.findIndex(o => id == o.id), 1);
		});
	}
}