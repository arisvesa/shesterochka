import { Injectable } from "@angular/core";
import { Product } from "./product.model";
//import { StaticDataSource } from "./static.datasource";
import { RestDataSource } from "./rest.datasource";

@Injectable()
export class ProductRepository {
   private products: Product[] = [];
   private categories: string[] = [];
   private subCat: string[] = [];
   private thisSubCat: string[] = [];

   constructor(private dataSource: RestDataSource) {
      dataSource.getProducts().subscribe(data => {
         this.products = data;
         this.categories = data.map(p => p.categories)
            .filter((c, index, array) => array.indexOf(c) == index).sort();
         this.subCat = data.map(p => p.subCat)
            .filter((c, index, array) => array.indexOf(c) == index).sort();
      });
   }
   getProducts(categories: string = null, subCat: string = null): Product[] {
      return this.products
         .filter(p => ((categories == null || categories == p.categories) && (subCat == p.subCat || subCat == null)));
   }

   getProduct(id: number): Product {
      return this.products.find(p => p.id == id);
   }

   getCategories(): string[] {
      return this.categories;
   }

   getSubcategories(categories: string): string[] {
      var products = Object.assign([], this.products);
      this.thisSubCat = products.filter((c, index, array) => c.categories == categories)
         .map(p => p.subCat)
         .filter((c, index, array) => array.indexOf(c) == index).sort();
      return this.thisSubCat;
   }

   saveProduct(product: Product) {
      if (product.id == null || product.id == 0) {
         this.dataSource.saveProduct(product)
            .subscribe(p => this.products.push(p));
      } else {
         this.dataSource.updateProduct(product)
            .subscribe(p => {
               this.products.splice(this.products.
                  findIndex(p => p.id == product.id), 1, product);
            });
      }
   }
   deleteProduct(id: number) {
      this.dataSource.deleteProduct(id).subscribe(p => {
         this.products.splice(this.products.
            findIndex(p => p.id == id), 1);
      })
   }
}