export class Slide {
 constructor(
	 public id?: number,
	 public src?: string,
	 public alt?: string) { }
}