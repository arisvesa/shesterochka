import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SuiModule } from 'ng2-semantic-ui';

import { AppComponent } from './app.component';
import { StoreModule } from "./store/store.module";
import { StoreComponent } from "./store/store.component";
import { CheckoutComponent } from "./store/checkout.component";
import { HelpComponent } from "./store/help.component";
import { CartDetailComponent } from "./store/cartDetail.component";
import { RouterModule } from "@angular/router";
import { StoreFirstGuard } from "./storeFirst.guard";

@NgModule({
	imports: [
		SuiModule,
		BrowserModule,
		StoreModule,
		RouterModule.forRoot(
			[
				{
					path: "store", component: StoreComponent,
					canActivate: [StoreFirstGuard]
				},
				{
					path: "cart", component: CartDetailComponent,
					canActivate: [StoreFirstGuard]
				},
				{
					path: "help/:section", component: HelpComponent,
					canActivate: [StoreFirstGuard]
				},
				{
					path: "admin",
					loadChildren: "./admin/admin.module#AdminModule",
					canActivate: [StoreFirstGuard]
				},
				{
					path: "**", redirectTo: "/store"
				}
			]
		)],
	exports: [RouterModule],
	providers: [StoreFirstGuard],
	declarations: [AppComponent],
	bootstrap: [AppComponent]
})
export class AppModule { }