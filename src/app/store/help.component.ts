import { Component, OnInit, OnDestroy  } from "@angular/core";
import { ActivatedRoute } from '@angular/router';

@Component({
	templateUrl: "help.component.html"
})

export class HelpComponent implements OnInit, OnDestroy { 
	private section: string;
	private sub: any;

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.section = params['section'];
		});
	}
	
	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}