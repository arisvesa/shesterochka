import { Component } from "@angular/core";
import { SlideRepository } from "../model/slide.repository";

@Component({
	selector: "slide-show",
	templateUrl: "slideShow.component.html",
	styleUrls: ["slideShow.component.css"]
})

export class SlideShowComponent {
	constructor(private repository: SlideRepository) { }

	get slides(): any[] {
		return this.repository.getSlides();
	}
}