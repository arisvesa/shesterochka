import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Product } from "../model/product.model";
import { ProductRepository } from "../model/product.repository";

@Component({
	selector: "sidebar",
	templateUrl: "sidebar.component.html"
})
export class SidebarComponent {

	public selectedCategory = null;
	public previousCategory = null;
	public selectedSubcategory = null;


	@Output() categoryEvent = new EventEmitter<string>();
	@Output() subcatEvent = new EventEmitter<string>();
	@Output() prevcatEvent = new EventEmitter<string>();

	sendCategory() {
		this.categoryEvent.emit(this.selectedCategory)
	}
	sendSubcat() {
		this.subcatEvent.emit(this.selectedSubcategory)
	}

	constructor(private repository: ProductRepository) {}

	get categories(): string[] {
		return this.repository.getCategories();
	}
	get subcategories(): string[] {
		return this.repository.getSubcategories(this.selectedCategory);
	}
	changeCategory(newCategory?: string) {
		if (this.previousCategory != newCategory){
			this.selectedSubcategory = null;
		};
		this.selectedCategory = newCategory;
		this.previousCategory = newCategory;	}
	changeSubcategory(newSubcategory?: string) {
		this.selectedSubcategory = newSubcategory;
	}
}