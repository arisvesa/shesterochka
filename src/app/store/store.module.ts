import { NgModule } from "@angular/core";
import { SuiModule } from 'ng2-semantic-ui';

import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { ModelModule } from "../model/model.module";
import { StoreComponent } from "./store.component";
import { StoreHeaderComponent } from "./storeHeader.component";
import { StoreFooterComponent } from "./storeFooter.component";
import { CounterDirective } from "./counter.directive";
import { CartSummaryComponent } from "./cartSummary.component";
import { SlideShowComponent } from "./slideShow.component";
import { CartDetailComponent } from "./cartDetail.component";
import { CheckoutComponent } from "./checkout.component";
import { HelpComponent } from "./help.component";
import { MessageFormComponent } from "./messageForm.component";
import { SidebarComponent } from "./sidebar.component";
import { ProductListComponent } from "./productList.component";
import { RouterModule } from "@angular/router";


@NgModule({
	imports: [SuiModule, ModelModule, BrowserModule, FormsModule, RouterModule],
	declarations: [
		StoreComponent,
		StoreHeaderComponent,
		StoreFooterComponent,
		CounterDirective, 
		CartSummaryComponent, 
		CartDetailComponent, 
		SlideShowComponent, 
		CheckoutComponent, 
		HelpComponent, 
		SidebarComponent,
		ProductListComponent,
		MessageFormComponent
	],
	exports: [StoreComponent, CartDetailComponent, CheckoutComponent, HelpComponent]
})
export class StoreModule { }