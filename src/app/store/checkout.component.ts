import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { OrderRepository } from "../model/order.repository";
import { Order } from "../model/order.model";
import { Cart } from "../model/cart.model";

@Component({
	selector: "checkout",
	templateUrl: "checkout.component.html"
})

export class CheckoutComponent {
	orderSent: boolean = false;
	submitted: boolean = false;
	constructor(public cart: Cart,
		public repository: OrderRepository,
		public order: Order) {}
	submitOrder(form: NgForm) {
		this.submitted = true;
		if (form.valid) {
			this.repository.saveOrder(this.order).subscribe(order => {
				this.order.clear();
				this.orderSent = true;
				this.submitted = false;
			});
		}
	}
}