import { Component, Input, EventEmitter, HostListener } from '@angular/core';
import { Product } from "../model/product.model";
import { ProductRepository } from "../model/product.repository";
import { Cart } from "../model/cart.model";

@Component({
	selector: "product-list",
	templateUrl: "productList.component.html"
})
export class ProductListComponent{

	@Input() selectedCategory: string = null;
	@Input() selectedSubcategory: string = null;
	
	public productsPerPage = 10;
	public selectedPage = 1;
	public currentProduct = null;


	constructor(private repository: ProductRepository,
 		private cart: Cart) { }

	get products(): Product[] {
		let pageIndex = (this.selectedPage - 1) * this.productsPerPage
		return this.repository.getProducts(this.selectedCategory, this.selectedSubcategory)
			.slice(pageIndex, pageIndex + this.productsPerPage);
	}
	get currentproduct(): Product[] {
		return this.currentProduct;
	}
	changePage(newPage: number) {
		this.selectedPage = newPage;
	}
	changePageSize(newSize: number) {
		this.productsPerPage = Number(newSize);
		this.changePage(1);
	}
	get pageCount(): number {
		return Math.ceil(this.repository
			.getProducts(this.selectedCategory).length / this.productsPerPage)
	}
	addProductToCart(product: Product) {
 		this.cart.addLine(product);
 	}
 	openCard(product: Product) {
 		this.currentProduct=Object.assign({}, product);
 	}
 	closeCard(product: Product) {
 		this.currentProduct=null;
 	}

}