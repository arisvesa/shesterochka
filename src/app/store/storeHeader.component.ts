import { Component, Input } from "@angular/core";

@Component({
	selector: "storeHeader",
	templateUrl: "storeHeader.component.html"
})

export class StoreHeaderComponent {

	@Input()
    headerFull: boolean = false;

	constructor() { }
}