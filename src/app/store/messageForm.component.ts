import { Component } from "@angular/core";

import { NgForm } from "@angular/forms";
import { MessageRepository } from "../model/message.repository";
import { Message } from "../model/message.model";


@Component({
	selector: "message-form",
	templateUrl: "messageForm.component.html"
})

export class MessageFormComponent {
    
    public messageSent: boolean = false;
    public submitted: boolean = false;
    
    constructor(
        public repository: MessageRepository,
        public message: Message
        ) { }
    
    submitMessage(form: NgForm) {
		this.submitted = true;
		if (form.valid) {
			this.repository.saveMessage(this.message).subscribe(message => {
				this.message.clear();
				this.messageSent = true;
				this.submitted = false;
			});
		}
	}
}